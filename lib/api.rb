module SLYU
  class API < Grape::API
    prefix "api"
 
    resource "buildings" do
      get do
        Building.all
      end
 
      get ':id' do
        Building.find(params[:id])
      end
    end
 
    resource "seats" do
      get ':id' do
        @classroom=Classroom.find(params[:id])
        present @classroom
      end
      get ':id/add' do
        @classroom=Classroom.find(params[:id])
        if (@classroom.freeseats < @classroom.seats)
          @classroom.freeseats +=1
          @classroom.building.freeseats +=1
          @classroom.building.save
          @classroom.save
        end
        present @classroom
      end
      get ':id/del' do
        @classroom=Classroom.find(params[:id])
        if !(@classroom.freeseats < 0)
          @classroom.freeseats -=1
          @classroom.building.freeseats -=1
          @classroom.building.save
          @classroom.save
        end
        present @classroom
      end
    end
    
    resource "sensors" do
      get do
        Sensor.all
      end
      get ':mac' do
        @sensor=Sensor.find(params[:mac])
      end

    end
 
  end
end