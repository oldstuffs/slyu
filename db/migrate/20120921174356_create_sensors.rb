class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :mac
      t.string :ip
      t.boolean :online

      t.timestamps
    end
  end
end
