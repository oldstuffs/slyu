class AddUseridNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :userid, :string, :null => false, :default => ""
    add_column :users, :name, :string, :null => false, :default => ""
  end
end
