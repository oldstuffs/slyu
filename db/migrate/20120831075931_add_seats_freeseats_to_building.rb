class AddSeatsFreeseatsToBuilding < ActiveRecord::Migration
  def change
    add_column :buildings, :seats, :integer, :default => 0
    add_column :buildings, :freeseats, :integer, :default => 0
  end
end
