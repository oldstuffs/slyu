class AddLightToClassroom < ActiveRecord::Migration
  def change
    add_column :classrooms, :light, :boolean
  end
end
