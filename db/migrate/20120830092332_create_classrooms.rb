class CreateClassrooms < ActiveRecord::Migration
  def change
    create_table :classrooms do |t|
      t.integer :floor
      t.integer :num
      t.integer :seats, :default => 0
      t.integer :freeseats, :default => 0
      t.integer :building_id

      t.timestamps
    end
  end
end
