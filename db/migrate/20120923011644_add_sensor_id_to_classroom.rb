class AddSensorIdToClassroom < ActiveRecord::Migration
  def change
    add_column :classrooms, :sensor_id, :integer
  end
end
