# coding: utf-8
require "bundler/capistrano"

require "rvm/capistrano"
# set :rvm_ruby_string, 'ruby-1.9.3-p194'
# set :rvm_type, :user
set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"") # Read from local system
set :rvm_type, :system  # Copy the exact line. I really mean :system here

set :application, "slyu"
set :repository,  "git://github.com/slyu/slyu.git"
set :branch, "master"
set :scm, :git
set :user, "slyu"
set :deploy_to, "/home/slyu/apps/#{application}"
set :runner, "slyu"
# set :deploy_via, :remote_cache
set :git_shallow_clone, 1

role :web, "106.186.16.131"                          # Your HTTP server, Apache/etc
role :app, "106.186.16.131"                          # This may be the same as your `Web` server
role :db,  "106.186.16.131", :primary => true        # This is where Rails migrations will run

# unicorn.rb 路径
set :unicorn_path, "#{deploy_to}/current/config/unicorn.rb"

namespace :deploy do
  task :start, :roles => :app do
    run "cd #{deploy_to}/current/; RAILS_ENV=production bundle exec unicorn_rails -c #{unicorn_path} -D"
  end

  task :stop, :roles => :app do
    run "kill -QUIT `cat #{deploy_to}/current/tmp/pids/unicorn.pid`"
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "kill -USR2 `cat #{deploy_to}/current/tmp/pids/unicorn.pid`"
  end
end


task :init_shared_path, :roles => :web do
  run "mkdir -p #{deploy_to}/shared/log"
  run "mkdir -p #{deploy_to}/shared/pids"
  run "mkdir -p #{deploy_to}/shared/assets"
  run "mkdir -p #{deploy_to}/shared/config"
end

task :link_shared_files, :roles => :web do
  run "ln -sf #{deploy_to}/shared/config/*.yml #{deploy_to}/current/config/"
  run "ln -sf #{deploy_to}/shared/config/unicorn.rb #{deploy_to}/current/config/"
  run "ln -sf #{deploy_to}/shared/config/initializers/secret_token.rb #{deploy_to}/current/config/initializers"
end

task :compile_assets, :roles => :web do
  run "cd #{deploy_to}/current/; RAILS_ENV=production bundle exec rake assets:precompile"
end

task :postgresql_migrate_database, :roles => :web do
  run "cd #{deploy_to}/current/; RAILS_ENV=production bundle exec rake db:migrate"
end

after "deploy:finalize_update","deploy:create_symlink", :init_shared_path, :link_shared_files, :compile_assets, :postgresql_migrate_database#,:sync_assets_to_cdn, :mongoid_migrate_database
