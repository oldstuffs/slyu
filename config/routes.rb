require 'api'
Slyu::Application.routes.draw do
  get "admin/index"

  resources :sensors do
    get 'online(/:mac)'=>'sensors#online', :on => :collection
  end

  resources :bulletins

  resources :articles

  resources :categories

  get "rooms/index"
  get "rooms/light(/:id)" => "rooms#light", :as=> :room_light
  get "findroom/index"

  get "findroom/search"

  get "maps/index(/:id)" => 'maps#index', :as => :maps

  mount SLYU::API => "/" #API will be available under "/api" url because of setting from MyApp::API line 4
  devise_for :users

  get "home/index"
  get "home/about"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  namespace :admin do
    # Directs /admin/products/* to Admin::ProductsController
    # (app/controllers/admin/products_controller.rb)
    resources :buildings do
      resources :classrooms, :controller => 'building_classrooms'
      
      get 'import', :on => :collection
      post 'save_import', :on => :collection
    end
    resources :users do
      get 'generated_password', :on => :member
      get 'import', :on => :collection
      post 'save_import', :on => :collection
    end
  end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
