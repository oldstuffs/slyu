class Building < ActiveRecord::Base
  attr_accessible :latitude, :longitude, :name
  has_many :classrooms, :dependent => :destroy
  
  validates :name,  :presence => true,
                    :uniqueness => true
end
