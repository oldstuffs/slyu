class Sensor < ActiveRecord::Base
  attr_accessible :classroom_id, :ip, :mac, :online
end
