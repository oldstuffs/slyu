class Classroom < ActiveRecord::Base
  attr_accessible :building_id, :floor, :freeseats, :num, :seats
  belongs_to :building, :class_name => "Building", :foreign_key => "building_id"
  #has_one :sensor, :class_name => "Sensor", :foreign_key => "sensor_id"
  def roomno
    [floor, sprintf("%02d", num)].join()
  end
  validates :num,                 :length => { :minimum => 1 },
                                  :numericality => true,
                                  :presence => true
  validates :floor,               :numericality => { :only_integer => true, :greater_than_or_equal_to => 1 },
                                  :presence => true
  validates :seats,               :numericality => { :only_integer => true, :greater_than_or_equal_to => 10 },
                                  :presence => true
  validates_presence_of :building_id
end
