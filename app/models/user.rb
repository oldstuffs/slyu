class User < ActiveRecord::Base
  # attr_accessible :excel_file
  # mount_uploader :excel_file, ExcelUploader
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, 
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :userid, :name, :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
  
end
