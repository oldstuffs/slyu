class MapsController < ApplicationController
  def index
    @buildings = Building.all(:order=>"name")
    if params[:id].present?
      @building = Building.find(params[:id])
    end
  end
end
