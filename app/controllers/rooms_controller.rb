require 'open-uri'

class RoomsController < ApplicationController
  def index
    @buildings = Building.all(:order=>"name")
  end
  
  def light
    @room = Classroom.find(params[:id])
    @room.light =!@room.light
    
    if (@room.light)?(open("http://192.168.1.101/light/open")):(open("http://192.168.1.101/light/close"))
      respond_to do |format|
        if @room.update_attributes(params[:building])
          format.html { redirect_to rooms_index_path, notice: "Go" }
          format.json { head :no_content }
        else
          format.html { redirect_to rooms_index_path, notice: "Down" }
          format.json { render json: @building.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
          format.html { redirect_to rooms_index_path, notice: "Bad" }
          format.json { head :no_content }
      end
    end

  end
  
end
