class Admin::BuildingsController < ApplicationController
  before_filter :authenticate_user!
  
  # GET /buildings
  # GET /buildings.json
  def index
    @buildings = Building.all(:order=>"name")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @buildings }
    end
  end

  # GET /buildings/1
  # GET /buildings/1.json
  def show
    @building = Building.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @building }
    end
  end

  # GET /buildings/new
  # GET /buildings/new.json
  def new
    @building = Building.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @building }
    end
  end

  # GET /buildings/1/edit
  def edit
    @building = Building.find(params[:id])
  end

  # POST /buildings
  # POST /buildings.json
  def create
    @building = Building.new(params[:building])

    respond_to do |format|
      if @building.save
        format.html { redirect_to [:admin, @building], notice: t("notices.building.create") }
        format.json { render json: @building, status: :created, location: @building }
      else
        format.html { render action: "new" }
        format.json { render json: @building.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /buildings/1
  # PUT /buildings/1.json
  def update
    @building = Building.find(params[:id])

    respond_to do |format|
      if @building.update_attributes(params[:building])
        format.html { redirect_to [:admin, @building], notice: t("notices.building.update") }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @building.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /buildings/1
  # DELETE /buildings/1.json
  def destroy
    @building = Building.find(params[:id])
    @building.destroy

    respond_to do |format|
      format.html { redirect_to admin_buildings_url }
      format.json { head :no_content }
    end
  end
  
  
  def import
  end
  
  def save_import
    uploader = ExcelUploader.new
    uploader.store!(params[:excel_file])
    book = Spreadsheet.open "#{uploader.store_path}"
    sheet1 = book.worksheet 0
    sheet1.each 1 do |row|
      b = Building.find_or_create_by_name(row[0].to_s)
      b.classrooms.create(:floor  =>  row[1].to_i.to_s,
                          :num    =>  row[2].to_i.to_s,
                          :seats  =>  row[3].to_i.to_s,
                          :freeseats  =>  row[3].to_i.to_s)
      b.seats += row[3].to_i
      b.freeseats += row[3].to_i
      b.save
    end
    
    uploader.remove!
    redirect_to admin_buildings_path
  end  
end
