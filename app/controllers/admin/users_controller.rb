class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @page_title=t('page_title.admin.users')
    @users = User.paginate(:page => params[:page], :per_page => 10, :order=>"userid")
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
    
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def generated_password
    redirect_to :action=>:edit, :id=>params[:id], :gened_pass=>Devise.friendly_token.first(6)
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])
    if !request[:password].blank?
      @gened_pass = request[:password]
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_users_path, notice: t('notices.users.update') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: admin_users_path.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
        format.html { redirect_to admin_users_path }
        format.js
    end
  end
  
  def import
  end
  
  def save_import
    uploader = ExcelUploader.new
    uploader.store!(params[:excel_file])
    book = Spreadsheet.open "#{uploader.store_path}"
    sheet1 = book.worksheet 0
    @users = []
    sheet1.each 1 do |row|
      p = User.new
      p.userid = row[0].to_i
      p.name = row[1]
      p.email = row[2].to_s
      if row[3].is_a?(Float)
        p.password = row[3].to_i.to_s
      else
        p.password = row[3].to_s
      end
      p.save
      #@users << p
    end
    
    uploader.remove!
    redirect_to admin_users_path
  end
end
