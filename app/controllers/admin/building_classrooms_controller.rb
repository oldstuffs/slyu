class Admin::BuildingClassroomsController < ApplicationController
  before_filter :find_building
  before_filter :authenticate_user!
  
  def index
    @classrooms = @building.classrooms(:order=>"floor,num")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @classrooms }
    end    
  end

  def show
    @classroom = @building.classrooms.find( params[:id] )

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @classrooms }
    end
  end

  def new
    @classroom = @building.classrooms.build

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @classrooms }
    end
  end

  def create
    @classroom = @building.classrooms.build( params[:classroom] )
    @classroom.freeseats = @classroom.seats
    @classroom.building.seats += @classroom.seats.to_i
    @classroom.building.freeseats += @classroom.seats.to_i
    @classroom.building.save
    respond_to do |format|
      if @classroom.save
        format.html { redirect_to admin_building_path( @building ), notice: t("notices.building_classroom.create") }
        format.json { render json: @building, status: :created, location: @building }
      else
        format.html { render action: "new" }
        format.json { render json: @building.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @classroom = @building.classrooms.find( params[:id] )
  end

  def update
    @classroom = @building.classrooms.find( params[:id] )
    respond_to do |format|
      if @building.update_attributes(params[:building])
        format.html { redirect_to admin_building_path( @building ), notice: t("notices.building_classroom.update") }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @building.errors, status: :unprocessable_entity }
      end
    end    
  end

  def destroy
    @classroom = @building.classrooms.find( params[:id] )
    @classroom.destroy
  

    respond_to do |format|
      format.html { redirect_to admin_building_path( @building ) }
      format.json { head :no_content }
    end
  end

  protected

  def find_building
    @building = Building.find( params[:building_id] )
  end
end
