class FindroomController < ApplicationController
  def index
    @q = Building.search(params[:q])
    @building = @q.result(:distinct => true)
  end

  def search
  end
end
